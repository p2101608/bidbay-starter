import express, { application } from 'express'
import { Product, Bid, User } from '../orm/index.js'
import authMiddleware from '../middlewares/auth.js'
import { getDetails } from '../validators/index.js'
import { InstanceError } from 'sequelize'

const router = express.Router()

router.get('/api/products', async (req, res, next) => {
  try {
    const products = await Product.findAll({
      attributes: ['id', 'name', 'description', 'category', 'originalPrice', 'pictureUrl', 'endDate'],
      include: [{ model: User, as: 'seller', attributes: ['id', 'username'] }, { model: Bid, as: 'bids', attributes: ['id', 'price', 'date'] }]
    })
    res.json(products).status(200)
  } catch (error) {
    res.json({ error }).status(404)
  }
})

router.get('/api/products/:productId', async (req, res) => {
  try {
    const id = req.params.productId
    const product = await Product.findByPk(id, {
      attributes: ['id', 'name', 'description', 'category', 'originalPrice', 'pictureUrl', 'endDate'],
      include: [
        { model: User, as: 'seller', attributes: ['id', 'username'] },
        { model: Bid, as: 'bids', attributes: ['id', 'price', 'date'], include: [{ model: User, as: 'bidder', attributes: ['id', 'username'] }] }]
    })
    if (product === null) {
      res.status(404).json({ error: 'Product not found' })
    } else {
      res.status(200).json(product)
    }
  } catch (error) {
    console.log(error)
    res.json({ error }).status(400)
  }
})

// You can use the authMiddleware with req.user.id to authenticate your endpoint ;)

router.post('/api/products', authMiddleware, async (req, res) => {
  const data = req.body
  data.sellerId = req.user.id
  try {
    const product = await Product.create(data)
    res.status(201).json(product).send()
  } catch (errors) {
    const msg = () => {
      const list = []
      for (let i = 0; i < errors.errors.length; i++) {
        const element = errors.errors[i]
        list.push(element.path)
      }
      return list
    }
    res.status(400).json({ error: 'Invalid or missing fields', details: msg() }).send()
  }
})

router.put('/api/products/:productId', authMiddleware, async (req, res) => {
  const id = req.params.productId
  const data = req.body
  const user = req.user.id
  data.sellerId = user
  try {
    const product = await Product.findByPk(id)
    if (product === null) {
      res.status(404).json({ error: 'Product not found' }).send()
    }
    if (product.sellerId === user || req.user.admin) {
      product.update(data)
      res.status(200).json(product).send()
    } else {
      res.status(403).json({ error: 'User not granted' }).send()
    }
  } catch (errors) {
    console.log(errors)
    if (errors instanceof InstanceError) {
      const fields = () => {
        const list = []
        errors.errors.forEach(element => {
          list.push(element.path)
        })
        return list
      }
      res.status(400).json({ error: 'Invalid or missing fields', details: fields() }).send()
    }
  }
})

router.delete('/api/products/:productId', authMiddleware, async (req, res) => {
  const idProduct = req.params.productId
  const user = req.user
  try {
    const product = await Product.findByPk(idProduct)
    if (product.sellerId === user.id || user.admin) {
      await product.destroy({
        where: {
          id: idProduct
        }
      })
      res.status(204).send()
    } else {
      res.status(403).json({ error: 'User not granted' }).send()
    }
  } catch (errors) {
    res.status(404).json({ error: 'Product not found' }).send()
  }
})

export default router
