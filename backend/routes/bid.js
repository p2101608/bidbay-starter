import authMiddleware from '../middlewares/auth.js'
import { Bid, Product } from '../orm/index.js'
import express from 'express'
import { getDetails } from '../validators/index.js'

const router = express.Router()

router.delete('/api/bids/:bidId', authMiddleware, async (req, res) => {
  const data = req.body
  res.status(404).send()
})

router.post('/api/products/:productId/bids', authMiddleware, async (req, res) => {
  const data = req.body
  const idProduct = req.params.productId
  try {
    const product = await Product.findByPk(idProduct)
    if (product === null) {
      res.status(404).json({ error: 'Product not found' }).send()
    }
    const bid = await Bid.create(data)
    res.status(201).json(bid).send()
  } catch (errors) {
    if (errors instanceof TypeError) {
      res.status(400).json({ error: 'Invalid or missing fields', details: ['price'] }).send()
    }
  }
})

export default router
